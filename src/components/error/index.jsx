import React, { Component } from 'react';
import ErrorLayout from './Error';

class Error extends Component {

  state = {
    hasError:false
  }


  componentDidCatch(error,info){
    this.setState({hasError:true})
  }

  render(){
    return(
      this.state.hasError
        ? <ErrorLayout />
        : this.props.children
    )
  }


}


export default Error;
