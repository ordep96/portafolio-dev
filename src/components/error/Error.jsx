import React from 'react';

import '../../styles/error.css';

import Calabera from '../../media/skull.svg';

const ErrorLayout = props => (
  <section className="content-error">
    <article className="error">
      <img src={Calabera} alt="error"/>
      <h2 style={{marginTop:'20px'}}>SHIT !!!, something went wrong</h2>
    </article>
  </section>
);


export default ErrorLayout;