import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import Home from './pages/Home/';
import ViewProject from './pages/Home/viewProject/'
import Admin from './pages/Protected/admin/';
import Login from './pages/Login/';
import Page404 from './pages/Page404';

import LoaderPrincipal from './widgets/LoaderPrincipal';



import { firebaseAuth } from '../data/config';

const PrivateRoute = ( { component:Component, authed, rest } ) => (
  <Route
    {...rest}
    render={
      props => authed === true
        ? <Component {...props} />
        : <Redirect to={ { pathname:'/me/login', state:{from:props.location} } } />
    }
  />
);

const PublicRoute = ( { component:Component, authed, rest } ) =>(
  <Route
    {...rest}
    render={
      props => authed === false
        ? <Component {...props} />
        : <Redirect to="/me/ordep-admin" />
    }
  />
);


class App extends Component{
  constructor(...props){
    super(...props)

    this.state = {
      authed:false,
      loading:true
    }
  }


  componentDidMount(){

    firebaseAuth().setPersistence(firebaseAuth.Auth.Persistence.SESSION);

    firebaseAuth().onAuthStateChanged(user => {
      if(user){
          this.setState({authed:true,loading:false})
      }else{
         this.setState({authed:false,loading:false})
      }

    })
  }



  render(){
   return(
     this.state.loading
      ? <LoaderPrincipal />
      :(
        <Router>
          <div>
            <Switch>
                <Route exact path="/me/" component={Home}/>
                <PrivateRoute authed={this.state.authed} path="/me/ordep-admin/" component={Admin}/>
                <PublicRoute authed={this.state.authed} path="/me/login" component={Login} />
                <Route path="/me/:name/" component={ViewProject} />
                <Route path='**' component={Page404} />
                <Route component={Page404} />
            </Switch>
          </div>
        </Router>
      )
   )
  }

}

export default App;