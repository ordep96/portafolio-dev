import React from 'react';
import uid from 'uid';
import MultiSelect from '../widgets/MultiSelect';
import { techs } from '../../../../../data/';


import '../../../../../styles/admin/addProyect.css';
import '../../../../../styles/admin/form.css';


const optionsTech = techs.map( tech => Object.assign( {},
  { label:tech, value:tech} ) )

const AddProject = props => (
  <section className="add-projects-content form-content">
    <form action="#" className="form-ui" onSubmit={props.addProject}>
      <label htmlFor="title">Titulo del Proyecto</label>
      <input className="form-ui__field" type="text" name="title" id="title"/>
      <MultiSelect
        name="techs"
        className="form-ui__field"
        placeholder="Elige las tecnologias"
        options={optionsTech}
      />
      <label htmlFor="subtitle">Subtitulo</label>
      <input className="form-ui__field" type="text" name="subtitle" id="subtitle" />

      <label htmlFor="cover">Portada Principal</label>
      <input onChange={props.uploadImage} className="form-ui__field" type="file" name="cover" id="cover"/>

      <label htmlFor="firstDescription">Primera Descripción</label>
      <textarea className="form-ui__field textarea" name="firstDescription" id="firstDescription" cols="30" rows="10"></textarea>

      <label htmlFor="imageDesktop">Imagen de Proyecto version Desktop</label>
      <input onChange={props.uploadImage} className="form-ui__field" type="file" name="imageDesktop" id="imageDesktop"/>

      <label htmlFor="secondDescription">Segunda Descripción</label>
      <textarea className="form-ui__field textarea" name="secondDescription" id="secondDescription" cols="30" rows="10"></textarea>

      <label htmlFor="imageMobile">Imagen de Proyecto version Mobile</label>
      <input onChange={props.uploadImage} className="form-ui__field" type="file" name="imageMobile" id="imageMobile"/>

      <label htmlFor="urlProyect">Url del Proyecto</label>
      <input className="form-ui__field" type="text" name="urlProyect" id="urlProyect"/>

      <input type="hidden" name="id" value={uid(8)}/>
      <input className="form-ui__field form-btn" type="submit"/>
    </form>
    {props.postedPost ? <h3 style={{color:"#fff"}}>Publicación exitosa</h3> : ""}
  </section>
);

export default AddProject;