import React from 'react';
import MultiSelect from '../widgets/MultiSelect';
import { techs } from '../../../../../data/';

import '../../../../../styles/admin/addProyect.css';
import '../../../../../styles/admin/form.css';
import '../../../../../styles/admin/modal.css';


const optionsTech = techs.map( tech => Object.assign( {},
  { label:tech, value:tech} ) )

const imgStyle = {
  width:'200px'
}

const NotImage = 'https://www.allwoodstairs.com/wp-content/uploads/2015/07/Photo_not_available-4-300x300.jpg';


const ModalUpdateProject = props => {
  if(props.activeModal){
    const { title, techs, subtitle, cover, firstDescription, imageDesktop,secondDescription,imageMobile, urlProyect, postId} = props.dataUpdate[0];

    const techsSelected = techs.map(tech => Object.assign({},{label:tech,value:tech}))
      return(
        <div className={`content-modal ${props.activeModal ? 'isActive':''}`}>
          <button onClick={props.closeModal} className="modal-btn-close">x</button>
          <section className="form-content content-form-modal">

          <form action="#" className="form-ui" onSubmit={props.updateProject}>
            <label htmlFor="title">Titulo del Proyecto</label>
            <input className="form-ui__field" type="text" name="title" id="title"
              defaultValue={title}
            />

            <label htmlFor="">Tecnologias Asignadas:</label>
            <MultiSelect
              name="techs"
              className="form-ui__field"
              placeholder="Elige las Tecnologias"
              options={optionsTech}
              defaultValues={techsSelected}
            />
            <label htmlFor="subtitle">Subtitulo</label>
            <input className="form-ui__field" type="text" name="subtitle" id="subtitle" defaultValue={subtitle}/>

            <div>
              <img style={imgStyle} src={cover !== 'not-image' ? cover : NotImage} alt={title}/>
              <input type="hidden" name="currentCover" defaultValue={cover}/>
            </div>
            <label htmlFor="cover">Portada Principal</label>
            <input onChange={props.uploadImage} className="form-ui__field" type="file" name="cover" id="cover"/>

            <label htmlFor="firstDescription">Primera Descripción</label>
            <textarea className="form-ui__field textarea" name="firstDescription" id="firstDescription" cols="30" rows="10" defaultValue={firstDescription}></textarea>

            <div>
              <img style={imgStyle} src={imageDesktop !== 'not-image' ? imageDesktop : NotImage} alt={title}/>
              <input type="hidden" name="currentImageDesktop" defaultValue={imageDesktop}/>
            </div>
            <label htmlFor="imageDesktop">Imagen de Proyecto version Desktop</label>
            <input onChange={props.uploadImage} className="form-ui__field" type="file" name="imageDesktop" id="imageDesktop"/>

            <label htmlFor="secondDescription">Segunda Descripción</label>
            <textarea className="form-ui__field textarea" name="secondDescription" id="secondDescription" cols="30" rows="10" defaultValue={secondDescription}></textarea>

            <div>
              <img style={imgStyle} src={imageMobile !== 'not-image' ? imageMobile : NotImage} alt={title}/>
              <input type="hidden" name="currentImageMobile" defaultValue={imageMobile}/>
            </div>
            <label htmlFor="imageMobile">Imagen de Proyecto version Mobile</label>
            <input onChange={props.uploadImage}  className="form-ui__field" type="file" name="imageMobile" id="imageMobile"/>

            <label htmlFor="urlProyect">Url del Proyecto</label>
            <input className="form-ui__field" type="text" name="urlProyect" id="urlProyect" defaultValue={urlProyect}/>

            <input type="hidden" name="id" defaultValue={postId}/>
            <input className="form-ui__field form-btn" type="submit"/>
          </form>
          </section>
        </div>
      )
  }else{
    return null
  }

};


export default ModalUpdateProject;