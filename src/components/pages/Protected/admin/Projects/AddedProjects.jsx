import React from 'react';

import '../../../../../styles/admin/addedProjects.css';

const AddedProjects = props => (
  <section className="proyect">
    <h2 className="title">Proyectos Añadidos</h2>
    {
      props.projects.length > 0
        ? (
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name Proyects</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </thead>
              <tbody>
                {
                  props.projects.map(project => (
                    <tr key={project.postId}>
                      <td>{project.postId}</td>
                      <td>{project.title}</td>
                      <td>{project.subtitle}</td>
                      <td>
                        <button className="btn-edit" index={project.postId} onClick={props.openModal}><i className="flaticon-edit"></i></button>
                        <button className="btn-deleted" index={project.postId} onClick={props.deleteProject}><i className="flaticon-delete-button"></i></button>
                      </td>
                    </tr>
                  )).reverse()
                }
              </tbody>
          </table>
        </div>
        )
       : <h2>No tienes Proyectos Cargados</h2>
    }
  </section>
);


export default AddedProjects;