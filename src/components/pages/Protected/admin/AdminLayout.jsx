import React from 'react';

const AdminLayout = props => (
  <section className="wrapper-admin case-page">
    { props.children }
  </section>
);


export default AdminLayout;