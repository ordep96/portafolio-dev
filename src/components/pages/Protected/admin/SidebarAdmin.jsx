import React from 'react';
import { Link } from 'react-router-dom';

import '../../../../styles/admin/sidebarAdmin.css';

const SidebarAdmin = props => (
  <aside
    className={`sidebar ${props.activeMenu ? 'open-menu': '' }`}  id="sidebar">
    <header className="sidebar__logo">
      <h2>Ordep96</h2>
    </header>
    <section className="sidebar__body">
      <ul className="sidebar__body__menu">
        <li>
          <Link to="/me/ordep-admin/add-project">
            <i className="flaticon-add"></i> Añadir Proyecto
          </Link>
        </li>
        <li>
            <Link to="/me/ordep-admin/projects">
              <i className="flaticon-binoculars"></i> Ver Proyectos
            </Link>
        </li>
        <li>
           <Link to="/me/ordep-admin/profile">
            <i className="flaticon-experiment"></i> Perfil
           </Link>
        </li>
        <li>
           <Link to="/me/ordep-admin/skills">
            <i className="flaticon-skills"></i> Skills
           </Link>
        </li>
        <li>
           <Link to="/me/ordep-admin/sociales">
            <i className="flaticon-facebook"></i> Redes Sociales
           </Link>
        </li>

        <li><a onClick={props.logout} href="/"><i className="flaticon-exit"></i> Salir</a></li>
      </ul>
    </section>
  </aside>
);


export default SidebarAdmin;