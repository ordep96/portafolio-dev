import React, { Component } from 'react';
import uid from 'uid';
import PropTypes from 'prop-types';
import{
  Route,
  Switch
} from 'react-router-dom';

import AdminLayout from './AdminLayout';
import SidebarAdmin from './SidebarAdmin';
import HeaderResponsive from './HeaderResponsive';
import WrapperAdmin from './WrapperAdmin';

import AddedProjects from './Projects/AddedProjects';
import AddProject from './Projects/AddProject';
import Profile from './Profile';
import AddSkills from './Skills/AddSkills';
import SocialMedia from './SocialMedia/SocialMedia';



/*  Modals */
import ModalContainer from '../../../widgets/ModalContainer';
import ModalUpdateProject from './Projects/ModalUpdateProject';
import ModalUpdateSkill from './Skills/ModalUpdateSkill';
import ModalUpdateSocial from './SocialMedia/ModalUpdateSocial';

/*  Loaders */
import LoaderPrincipal from '../../../widgets/LoaderPrincipal';
import LoaderFile from './widgets/LoaderFile';

import {
    firebaseAuth,
    firebaseStorage,
    firebaseDatabase
} from '../../../../data/config';

import {
  Logout,
  writeUserData,
  writePostPortfolio,
  updatePostPortfolio,
  writeSkill,
  updateSkill,
  writeSocialMedia,
  updateSocial,
  deleteImgs,
} from '../../../helpers/Auth';


class Admin extends Component{
    state = {
      activeMenu:false,
      user:null,

      loading:true,
      loadingFile:false,
      progressUploadFile:0,

      userUpdate:false,
      userInfo:[],
      userImgProfile:[],

      listProjects:[],
      imgProjects:[],

      postedPost:false,
      activeModalProject:false,

      dataUpdate:[],
      messageLoader:'',
      curriculum:[],

      /* State for Skills */
      skills:[],
      activeModalSkill:false,

      /* State for Social */
      socials:[],
      activeModalSocial:false
    }


  handleOpenMenu = (e) => {
    e.preventDefault();
    this.setState({activeMenu:true});
  }

  handleCloseMenu = (e) => {
    e.preventDefault();
    this.setState({activeMenu:false})
  }

  handleLogout = (e) =>{
    e.preventDefault();
    Logout();
  }

  /* funcion para subir Curriculum */
  handleUploadCurriculum = (e) =>{
    let file = e.target.files[0];

    let storageRef = firebaseStorage().ref(`file/${file.name}`),
        uploadCurriculum = storageRef.put(file);

    uploadCurriculum.on('state_changed', snapshot =>{
        let progress = Math.floor((snapshot.bytesTransferred / snapshot.totalBytes) * 100);

        this.setState({
          loadingFile:true,
          messageLoader:'Subiendo Curriculum',
          progressUploadFile:progress
        })
    }, error => {
      console.log(error.message)
    }, () => {
      uploadCurriculum.snapshot.ref.getDownloadURL()
        .then(downloadUrl => {
          this.setState({
            curriculum:downloadUrl,
            loadingFile:false
          })
        })
    })

  }

  /* Funcion para subir las imagenes en el formulario de Añadir y actualizar*/
  handleUploadImage = (e) =>{
    let image = e.target.files[0],
        name = e.target.name;

    let storageRef = firebaseStorage().ref(`images/${image.name}`),
        uploadImg = storageRef.put(image);

    uploadImg.on('state_changed', snapshot => {
      let progress = Math.floor((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
      this.setState({
        loadingFile:true,
        messageLoader:'Cargando Imagen',
        progressUploadFile:progress
      })
    }, error => {
      console.log(error.message)
    }, () => {
        uploadImg.snapshot.ref.getDownloadURL()
          .then(downloadUrl => {

            switch(name){
              case "cover":
                let cover = {"cover":downloadUrl}
                this.setState({imgProjects:Object.assign(this.state.imgProjects,cover),loadingFile:false})
                break;
              case "imageDesktop":
                let imgDesktop = {"imageDesktop":downloadUrl}
                this.setState({imgProjects:Object.assign(this.state.imgProjects,imgDesktop),loadingFile:false})
                break;
              case "imageMobile":
                let imgMobile = {"imageMobile":downloadUrl}
                this.setState({imgProjects:Object.assign(this.state.imgProjects,imgMobile),loadingFile:false})
                break;
              case "imageProfile":
                let imgProfile = {"imageProfile":downloadUrl}
                this.setState({
                  userImgProfile:Object.assign(this.state.userImgProfile,imgProfile),
                  loadingFile:false
                })
                break;
              default:
                this.setState({
                  imgProjects:{},
                  userImgProfile:{}
                })
            }
        })
    })

  }

  /* Subimos la Actualizacion de perfil */
  handleUpdateProfile = (e) =>{
    e.preventDefault();
    let form = e.target,
         datos = {
            imgUser:(this.state.userImgProfile.imageProfile) ? (this.state.userImgProfile.imageProfile) ? this.state.userImgProfile.imageProfile : Admin.defaultProps.imgUser : (form.currentImageProfile.value) ? form.currentImageProfile.value : Admin.defaultProps.imgUser,

            name:(form.name.value) ? form.name.value : Admin.defaultProps.name,
            userName:(form.username.value) ? form.username.value : Admin.defaultProps.userName,
            career:(form.career.value) ? form.career.value : Admin.defaultProps.career,
            hiddenName:form.hiddenName.value,

            curriculum:(this.state.curriculum) ? (this.state.curriculum) ? this.state.curriculum : Admin.defaultProps.curriculum : (form.currentCurriculum.value) ? form.currentCurriculum.value : Admin.defaultProps.curriculum
          }



          this.state.user.updateProfile({
              displayName:datos.name,
              photoURL:datos.imgUser
          })
          .then(() => {
            writeUserData(datos.name,datos.userName,datos.career,datos.imgUser,datos.curriculum,datos.hiddenName)
              this.setState({userUpdate:true})
          })
  }


  /* Añadimos Proyectos desde la vista de AddProjects */
  handleAddProject = (e) => {
    e.preventDefault();
    let form = e.target,
        datos = {
          id:(form.id.value) ? form.id.value : Admin.defaultProps.id,
          title:(form.title.value) ? form.title.value : Admin.defaultProps.title,
          techs:(form.techs.value) ? form.techs.value.split(',') : Admin.defaultProps.techs,
          subtitle:(form.subtitle.value) ? form.subtitle.value : Admin.defaultProps.subtitle,
          cover:(this.state.imgProjects.cover) ? this.state.imgProjects.cover : Admin.defaultProps.cover,
          firstDescription:(form.firstDescription.value) ? form.firstDescription.value : Admin.defaultProps.firstDescription,
          imageDesktop:(this.state.imgProjects.imageDesktop) ? this.state.imgProjects.imageDesktop : Admin.defaultProps.imageDesktop,
          secondDescription:(form.secondDescription.value) ? form.secondDescription.value : Admin.defaultProps.secondDescription,
          imageMobile:(this.state.imgProjects.imageMobile) ? this.state.imgProjects.imageMobile: Admin.defaultProps.imageMobile,
          urlProyect:(form.urlProyect.value) ? form.urlProyect.value : Admin.defaultProps.urlProyect
        }

      writePostPortfolio(datos.title,datos.techs,datos.subtitle,datos.cover,datos.firstDescription,datos.imageDesktop,datos.secondDescription,datos.imageMobile,datos.urlProyect,datos.id)
        .then(() => {
          form.reset();
          this.setState({
            postedPost:true,
            imgProjects:[]
          })
        })
  }

  /* Funcion para eliminar Proyectos añadidos */
  handleDeleteProject = (e) => {
    let id = e.target.getAttribute('index'),
        diferent = [];

    if(window.confirm("Seguro que quieres borrar este proyecto")){
      this.state.listProjects.filter(project => {
        if(project.postId === id){
          firebaseDatabase().ref(`portfolio/${id}`).remove();
          deleteImgs([project.cover,project.imageDesktop,project.imageMobile])
        }else{
           diferent = diferent.concat(project);
        }
        return this.setState({listProjects:diferent})
      });
    }

  }

  /* Actualización de Proyecto */
  handleUpdateProject = (e) =>{
    e.preventDefault();
    let form = e.target,
        datos = {
          postId:form.id.value,
          title:form.title.value,
          techs:form.techs.value.split(','),
          subtitle:form.subtitle.value,
          cover:(this.state.imgProjects.cover) ? (this.state.imgProjects.cover) ? this.state.imgProjects.cover : Admin.defaultProps.cover : (form.currentCover.value) ? form.currentCover.value : Admin.defaultProps.cover,

          /* (form.currentCover.value !== 'not-image') ? (form.currentCover.value) ? form.currentCover.value : Admin.defaultProps.cover : (this.state.imgProjects.cover) ? this.state.imgProjects.cover : Admin.defaultProps.cover */

          firstDescription:form.firstDescription.value,

          imageDesktop:(this.state.imgProjects.imageDesktop) ? (this.state.imgProjects.imageDesktop) ? this.state.imgProjects.imageDesktop : Admin.defaultProps.imageDesktop : (form.currentImageDesktop.value) ? form.currentImageDesktop.value : Admin.defaultProps.imageDesktop,

          secondDescription:form.secondDescription.value,

          imageMobile:(this.state.imgProjects.imageMobile) ? (this.state.imgProjects.imageMobile) ? this.state.imgProjects.imageMobile : Admin.defaultProps.imageMobile : (form.currentImageMobile.value) ? form.currentImageMobile.value : Admin.defaultProps.imageMobile,
          urlProyect:form.urlProyect.value
      }


    updatePostPortfolio(datos.title,datos.techs,datos.subtitle,datos.cover,datos.firstDescription,datos.imageDesktop,datos.secondDescription,datos.imageMobile,datos.urlProyect,datos.postId)
      .then(() => {
        alert("Actualización exitosa");

        this.state.listProjects.filter((project,i) => {
          if(project.postId === datos.postId){
            let proyects = Object.assign(this.state.listProjects);
              proyects[i] = datos;

              this.setState({
                listProjects:proyects,
                activeModalProject:false,
                imgProjects:[]
              })
          }

          return false

        });

      })
  }


  /* Anadir Habilidades  */
  handleAddSkill = (e) => {
    e.preventDefault();
    let form = e.target,
        datos = {
          skill:form.title.value,
          percent:form.percent.value,
          id:form.id.value
        }

     writeSkill(datos.skill,datos.percent,datos.id)
      .then(() => {
        alert("Habiliadad Añadida");
        form.reset();
      })
  }


  /* Eliminar una Habilidad */
  handleDeleteSkill = (e) => {
    e.preventDefault();
    let id = e.target.getAttribute('index'),
        diferent = [];

    if(window.confirm("Seguro que quieres borrar esta habilidad")){
      this.state.skills.filter(skill => {
        if(skill.skillId === id){
          firebaseDatabase().ref(`skills/${id}`).remove();
        }else{
           diferent = diferent.concat(skill);
        }
        return this.setState({skills:diferent})
      });
    }
  }

  /* Actualizar habilidad */
  handleUpdateSkill = (e) => {
    e.preventDefault();
    let form = e.target,
        datos = {
          skill:form.title.value,
          percent:form.percent.value,
          skillId:form.id.value
        }

    updateSkill(datos.skill,datos.percent,datos.skillId)
      .then(() => {
        alert("Actualizacion Exitosa")
        this.setState({activeModalSkill:false})
      })

  }


  /* abrir la modal y obtener los datos de la habilidad seleccionada*/
  handleOpenModalSkill = (e) => {
    let id = e.target.getAttribute('index'),
        data = this.state.skills.filter(skill => skill.skillId === id);

    this.setState({
      dataUpdate:data,
      activeModalSkill:true
    })
  }


  /*  Funcion para añadir una red Social */
  handleAddSocial = (e) => {
    e.preventDefault();
    let form = e.target,
        datos = {
          name:form.name.value,
          linkPerfil:form.linkPerfil.value,
          id:form.id.value
        }

    writeSocialMedia(datos.name,datos.linkPerfil,datos.id)
      .then(() => {
        alert("Red Social Añadida")

        form.reset();
      })

  }

  /* abrir la modal y obtener la data de la red social seleccionada */
  handleOpenModalSocial = (e) => {
    e.preventDefault();
    let id = e.target.getAttribute('index'),
        data = this.state.socials.filter(social => social.socialId === id);

    this.setState({
      dataUpdate:data,
      activeModalSocial:true
    })

  }

  /* Actualizar red social */
  handleUpdateSocial = (e) => {
    e.preventDefault();
    let form = e.target,
        datos = {
          name:form.name.value,
          urlSocial:form.urlSocial.value,
          id:form.id.value
        }

    updateSocial(datos.name,datos.urlSocial,datos.id)
      .then(() => {
        alert("Actualizacion Exitosa")
        this.setState({activeModalSocial:false})
      })
  }


  /* Eliminamos un red social */
  handleDeleteSocial = (e) => {
    e.preventDefault();
    let id = e.target.getAttribute('index'),
        diferent = [];

    if(window.confirm("Seguro que quieres borrar esta habilidad")){
      this.state.socials.filter(social => {
        if(social.socialId === id){
          firebaseDatabase().ref(`socials/${id}`).remove();
        }else{
           diferent = diferent.concat(social);
        }
        return this.setState({socials:diferent})
      });
    }
  }



  handleOpenModal = (e) =>{
    let id = e.target.getAttribute('index');
    let data = this.state.listProjects.filter(project => project.postId === id)
    this.setState({
      dataUpdate:data,
      activeModalProject:true
    })
  }

  handleCloseModal = (e) => {
    this.setState({activeModalProject:false,activeModalSkill:false})
  }


  componentDidMount(){

    this.removeListener = firebaseAuth().onAuthStateChanged(user => {
        if(user){
          this.setState({user,loading:false})
        }else{
          this.setState({user:null})
        }
    })

    this.refUser = firebaseDatabase().ref('user/ordep')
    this.refUser.once('value', snapshot => {
      this.setState({
        userInfo:snapshot.val()
      })
    })

    /* Obteniendo la lista de Habilidades  */
    this.refSkill = firebaseDatabase().ref('skills')
    this.refSkill.on('child_added', snapshot => {
      this.setState({
        skills:this.state.skills.concat([snapshot.val()])
      })
    })

      /* Detectamos si se actulizado una habilidad */
     this.refSkill.on('child_changed',snapshot => {
        let skillUpdate = snapshot.val();
        this.state.skills.filter((skill,i) => {
          if(skill.skillId === skillUpdate.skillId){
            let skills = Object.assign(this.state.skills);
                skills[i] = skillUpdate;
            this.setState({skills})
          }

          return false
        })
    })

    /* Obtenemos los datos de las redes sociales */
    this.refSocial = firebaseDatabase().ref('socials')
    this.refSocial.on('child_added', snapshot => {
      this.setState({socials:this.state.socials.concat([snapshot.val()])})
    })

    /* Detectamos si se actualiza una red social */
    this.refSocial.on('child_changed', snapshot => {
      let socialUpdated = snapshot.val();
      this.state.socials.filter((social,i) =>{
        if(social.socialId === socialUpdated.socialId){

          let socials = Object.assign(this.state.socials);
            socials[i] = socialUpdated;

            this.setState({socials})
        }
        return false
      })
    })


    /* Obtenemos los datos del portafolio */
    this.ref = firebaseDatabase().ref(`portfolio`)
    this.ref.on('child_added',snapshot => {
      this.setState({
        listProjects:this.state.listProjects.concat([snapshot.val()])
      })
    })

  }




  componentWillUnmount(){
    this.removeListener();
    this.ref.off();
    this.refUser.off();
    this.refSkill.off();
    this.refSocial.off();
  }

  render(){
    return(
      <AdminLayout>
        <LoaderPrincipal
          loading={this.state.loading}
        />
        <HeaderResponsive
          openMenu={this.handleOpenMenu}
          closeMenu={this.handleCloseMenu}
          activeMenu={this.state.activeMenu}
        />
        <SidebarAdmin
          activeMenu={this.state.activeMenu}
          logout={this.handleLogout}
        />
        <WrapperAdmin>
          <LoaderFile
            title={this.state.messageLoader}
            loading={this.state.loadingFile}
            progress={this.state.progressUploadFile}
          />
          <Switch>
            <Route exact path="/ordep-admin/" render={() => (
              <h2 style={{color:"#fff"}}>Bienvenido</h2>
            )} />
            <Route path="/me/ordep-admin/add-project/" render={() => (
              <AddProject
                addProject={this.handleAddProject}
                uploadImage={this.handleUploadImage}
                postedPost={this.state.postedPost}
                loadingImage={this.state.loadingImage}
              />
            )} />
            <Route path="/me/ordep-admin/projects/" render={() => (
              <AddedProjects
                projects={this.state.listProjects}
                deleteProject={this.handleDeleteProject}
                openModal={this.handleOpenModal}
              />
            )}/>
            <Route path="/me/ordep-admin/profile/" render={() => (
              <Profile
                user={this.state.user}
                updateProfile={this.handleUpdateProfile}
                userUpdate={this.state.userUpdate}
                uploadCurriculum={this.handleUploadCurriculum}
                userInfo={this.state.userInfo}
                uploadImage={this.handleUploadImage}
              />
            )}/>Project
            <Route path="/me/ordep-admin/skills/" render={() => (
              <AddSkills
                addSkill={this.handleAddSkill}
                skills={this.state.skills}
                deleteSkill={this.handleDeleteSkill}
                openModal={this.handleOpenModalSkill}
              />
            )}/>
            <Route path="/me/ordep-admin/sociales/" render={() => (
              <SocialMedia
                addSocial={this.handleAddSocial}
                socials={this.state.socials}
                deleteSocial={this.handleDeleteSocial}
                openModal={this.handleOpenModalSocial}
              />
            )}/>
          </Switch>
          <ModalContainer>
            <ModalUpdateProject
                activeModal={this.state.activeModalProject}
                closeModal={this.handleCloseModal}
                dataUpdate={this.state.dataUpdate}
                uploadImage={this.handleUploadImage}
                updateProject={this.handleUpdateProject}
            />
            <ModalUpdateSkill
              activeModal={this.state.activeModalSkill}
              closeModal={this.handleCloseModal}
              dataUpdate={this.state.dataUpdate}
              updateSkill={this.handleUpdateSkill}
            />
            <ModalUpdateSocial
              activeModal={this.state.activeModalSocial}
              closeModal={this.handleCloseModal}
              dataUpdate={this.state.dataUpdate}
              updateSocial={this.handleUpdateSocial}
            />
          </ModalContainer>
        </WrapperAdmin>
      </AdminLayout>
    )
  }

}


Admin.propTypes = {
  id: PropTypes.string.isRequired,
  title:PropTypes.string.isRequired,
  techs:PropTypes.string.isRequired,
  subtitle:PropTypes.string.isRequired,
  cover:PropTypes.string.isRequired,
  firstDescription:PropTypes.string.isRequired,
  imageDesktop:PropTypes.string.isRequired,
  secondDescription:PropTypes.string.isRequired,
  imageMobile:PropTypes.string.isRequired,
  urlProyect:PropTypes.string.isRequired,

  /* PropTypes info usuario */
  imgUser:PropTypes.string.isRequired,
  career:PropTypes.string.isRequired,
  name:PropTypes.string.isRequired,
  userName:PropTypes.string.isRequired,
  curriculum:PropTypes.string.isRequired

}

/* "https://www.allwoodstairs.com/wp-content/uploads/2015/07/Photo_not_available-4-300x300.jpg" */

Admin.defaultProps = {
  id: uid(8),
  title:"titulo no definido",
  techs:"Tecnologias no asignadas",
  subtitle:"Subtitulo no disponible",
  cover:"not-image",
  firstDescription:"Descripción no disponible",
  imageDesktop:"not-image",
  secondDescription:"Descripción no disponible",
  imageMobile:"not-image",
  urlProyect:'undefined',

  /* Default Props para info Usuario */
  imgUser:"not-image",
  name:"Nombre no definido",
  userName:"Usuario no definido",
  curriculum:"not-curriculum",
  career:"Carrera no definida"
}

export default Admin;