import React from 'react';

const AddedSocialMedia = props => (
<section className="proyect skills">
    <h2 className="title">Sociales Añadidas</h2>
    {
      props.socials.length > 0
        ? (
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Red Social</th>
                <th>Url</th>
                <th>Actions</th>
              </tr>
            </thead>
              <tbody>
                {
                  props.socials.map(social => (
                    <tr key={social.socialId}>
                      <td>{social.socialId}</td>
                      <td>{social.name}</td>
                      <td>{social.urlSocial}</td>
                      <td>
                        <button
                          className="btn-edit"
                          index={social.socialId}
                          onClick={props.openModal}><i className="flaticon-edit"></i>
                        </button>
                        <button
                          className="btn-deleted"
                          index={social.socialId}
                          onClick={props.deleteSocial}><i className="flaticon-delete-button"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
          </table>
        </div>
        )
       : <h2>No hay red Social Añadida</h2>
    }
  </section>
);



export default AddedSocialMedia;