import React from 'react';
import uid from 'uid';

import AddedSocialMedia from './AddedSocialMedia';

import '../../../../../styles/admin/form.css';

const SocialMedia = props => (
  <section className="add-projects-content form-content">
    <form action="#" className="form-ui" onSubmit={props.addSocial}>
      <label htmlFor="name">Nombre de la Red Social</label>
      <input className="form-ui__field" type="text" name="name" id="title"/>

      <label htmlFor="link">Link de tu Perfil</label>
      <input className="form-ui__field" type="url" name="linkPerfil" id="link" required/>

      <input type="hidden" name="id" value={ uid(5) }/>

      <input className="form-ui__field form-btn" type="submit" value="Agregar Red Social" />
    </form>

    <AddedSocialMedia
      socials={props.socials}
      deleteSocial={props.deleteSocial}
      openModal={props.openModal}
    />
  </section>
);


export default SocialMedia;