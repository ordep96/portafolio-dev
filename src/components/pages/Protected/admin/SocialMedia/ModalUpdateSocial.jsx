import React from 'react';

import '../../../../../styles/admin/form.css';
import '../../../../../styles/admin/modal.css';

const ModalUpdateSkill = props => {
  if(props.activeModal){
      const { name, socialId, urlSocial } = props.dataUpdate[0];

      return(
        <div className={`content-modal content-modal-skill ${props.activeModal ? 'isActive':''}`}>
          <button
            onClick={props.closeModal} className="modal-btn-close">x
          </button>
          <section className="form-content content-form-modal form-modal-skill">

            <form className="form-ui" onSubmit={props.updateSocial}>
              <label htmlFor="name">Nombre de la Red Social</label>
              <input className="form-ui__field" type="text" name="name" id="name" defaultValue={name} />

              <label htmlFor="urlSocial">Url de la red Social</label>
              <input type="url" className="form-ui__field" name="urlSocial" id="urlSocial" defaultValue={urlSocial}/>

              <input type="hidden" name="id" defaultValue={socialId}/>

              <input className="form-ui__field form-btn" type="submit" value="Actualizar Red Social"/>
            </form>
          </section>
        </div>
      )
  }else{
    return null
  }

};


export default ModalUpdateSkill;

