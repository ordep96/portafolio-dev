import React from 'react';
import uid from 'uid';

import AddedSkills from './AddedSkills';

import '../../../../../styles/admin/form.css';


const AddSkills = props => (
  <section className="add-projects-content form-content">
    <form action="#" className="form-ui" onSubmit={props.addSkill}>
      <label htmlFor="title">Nombre de la Habilidad</label>
      <input className="form-ui__field" type="text" name="title" id="title"/>

      <label htmlFor="percent">Porcentaje de la Habilidad</label>
      <input className="form-ui__field" type="text" name="percent" id="percent"/>

      <input type="hidden" name="id" value={ uid(5) }/>

      <input className="form-ui__field form-btn" type="submit" value="Agregar Skill" />
    </form>

    <AddedSkills
      skills={props.skills}
      deleteSkill={props.deleteSkill}
      openModal={props.openModal}
    />
  </section>
);


export default AddSkills;