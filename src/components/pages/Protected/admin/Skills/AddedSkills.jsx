import React from 'react';


const AddedSkills = props => (
  <section className="proyect skills">
    <h2 className="title">Skills Añadidas</h2>
    {
      props.skills.length > 0
        ? (
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name Skill</th>
                <th>Percent</th>
                <th>Actions</th>
              </tr>
            </thead>
              <tbody>
                {
                  props.skills.map(skill => (
                    <tr key={skill.skillId}>
                      <td>{skill.skillId}</td>
                      <td>{skill.skill}</td>
                      <td>{skill.percent}</td>
                      <td>
                        <button
                          className="btn-edit"
                          index={skill.skillId}
                          onClick={props.openModal}><i className="flaticon-edit"></i>
                        </button>
                        <button
                          className="btn-deleted"
                          index={skill.skillId}
                          onClick={props.deleteSkill}><i className="flaticon-delete-button"></i>
                        </button>
                      </td>
                    </tr>
                  ))
                }
              </tbody>
          </table>
        </div>
        )
       : <h2>No tienes Habilidades Agregadas</h2>
    }
  </section>

);


export default AddedSkills;