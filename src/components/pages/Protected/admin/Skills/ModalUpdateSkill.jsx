import React from 'react';

import '../../../../../styles/admin/form.css';
import '../../../../../styles/admin/modal.css';

const ModalUpdateSkill = props => {
  if(props.activeModal){
      const { percent, skill, skillId } = props.dataUpdate[0];

      return(
        <div className={`content-modal content-modal-skill ${props.activeModal ? 'isActive':''}`}>
          <button
            onClick={props.closeModal} className="modal-btn-close">x
          </button>
          <section className="form-content content-form-modal form-modal-skill">

            <form className="form-ui" onSubmit={props.updateSkill}>
              <label htmlFor="title">Nombre de la Habilidad</label>
              <input className="form-ui__field" type="text" name="title" id="title" defaultValue={skill} />

              <label htmlFor="percent">Porcentaje de la Habilidad</label>
              <input type="text" className="form-ui__field" name="percent" id="percent" defaultValue={percent}/>

              <input type="hidden" name="id" defaultValue={skillId}/>

              <input className="form-ui__field form-btn" type="submit" value="Actualizar Skill"/>
            </form>
          </section>
        </div>
      )
  }else{
    return null
  }

};


export default ModalUpdateSkill;