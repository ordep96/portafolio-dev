import React from 'react';

import userNotImage from '../../../../media/no_image.jpg';

import '../../../../styles/admin/form.css';
import '../../../../styles/admin/formEditProfile.css';


const Profile = props => {
  if(props.user ){
  return(
    <section style={{marginTop:'20px'}} className="form-wrapper wrapper-formProfile">
      <h2 className="title" style={{color:'#fff'}}>Editar Perfil</h2>
      <div className="edit-profile">
        <div className="form-content">
          <div className="profile-img">
            <img src={props.user.photoURL !== null ? props.user.photoURL : userNotImage } alt=""/>
          </div>
          <form className="form-ui" onSubmit={props.updateProfile}>
            <input onChange={props.uploadImage} className="form-ui__field" type="file" name="imageProfile"/>
            <input type="hidden" name="currentImageProfile" defaultValue={props.user.photoURL}/>

            <label htmlFor="name">Nombre</label>
            <input className="form-ui__field" type="text" id="name" name="name" defaultValue={props.user.displayName !== null ? props.user.displayName : 'Pedro Muñoz Alvear'} />

            <label htmlFor="username">Usuario</label>
            <input className="form-ui__field" type="text" name="username" placeholder="ordep96" id="username" defaultValue={props.userInfo.username}/>

            <input className="form-ui__field" type="text" name="career" placeholder="frontend"defaultValue={props.userInfo.career} />

            <label htmlFor="curriculum">Curriculum</label>
            <input onChange={props.uploadCurriculum} className="form-ui__field" type="file" name="curriculum" id="curriculum"/>

            <input type="hidden" name="currentCurriculum" defaultValue={props.userInfo.curriculum}/>

            <input type="hidden" name="hiddenName" value="ordep"/>

            <input className="form-ui__field form-btn" type="submit" value="actualizar perfil" />
          </form>
            { props.userUpdate ? <p>Perfil Actualizado</p> : ''}
        </div>
      </div>
    </section>
    )
  }else{
    return null
  }
};


export default Profile;