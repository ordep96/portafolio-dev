import React from 'react';

import '../../../../styles/admin/headerResponsive.css';

const HeaderResponsive = props => (
  <div className="menu-bar">
    <h2>Ordep96</h2>
    <button onClick={props.activeMenu ? props.closeMenu  : props.openMenu} className="btn-toggle" id="btn-menu">
      <i className="flaticon-menu"></i>
    </button>
  </div>
);


export default HeaderResponsive;