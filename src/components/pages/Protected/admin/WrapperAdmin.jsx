import React from 'react';

const WrapperAdmin = props => (
  <section className="wrapper-inner">
    <div className="ordep-container-admin">
      { props.children }
    </div>
  </section>
);


export default WrapperAdmin;