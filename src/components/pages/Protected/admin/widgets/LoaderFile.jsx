import React from 'react';

import iconLoader from '../../../../../media/outbox.svg';
import '../../../../../styles/admin/widget/loaderImage.css';

const LoaderFile = props => (
  <section className={`loader-image ${props.loading === true ? 'is-loading' : ''}`}>
   <img className="icon-loader" src={iconLoader} alt=""/>
   <h2>{props.title}</h2>
   <span>{props.progress} %</span>
  </section>
);


export default LoaderFile;