import React, { Component } from 'react'
import Select from 'react-select'
import 'react-select/dist/react-select.css'

class MultiSelect extends Component {

  constructor(...props){
    super(...props)

    this.state = {
      options:this.props.options,
      value:[]
    }

    this.handleSelectChange = this.handleSelectChange.bind(this)

  }



  handleSelectChange(value){
    this.setState({value})
  }



  render(){
    return(

      <Select
        multi={true}
        simpleValue={true}
        joinValues={true}
        placeholder={this.props.placeholder}
        name={this.props.name}
        value={(this.state.value.length > 0) ? this.state.value : (this.props.defaultValues) ? this.props.defaultValues.map(tech => tech): false}
        options={this.state.options}
        onChange={this.handleSelectChange}
      />

    )
  }

}


export default MultiSelect