import React from 'react';
import { Link } from 'react-router-dom';

import posion from '../../media/posion.svg';

import '../../styles/404.css';

const Page404 = props => (
  <section className="error-wrapper">
    <article className="error">
      <div className="error__title">
        <img src={posion} alt="posion" />
      </div>
      <p>Page not found !</p>
      <Link className="btn-home" to="/">Back to home</Link>
    </article>
  </section>
);


export default Page404;