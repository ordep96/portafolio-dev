import React, { Component } from 'react';
import { LoginUser } from '../../helpers/Auth';

import FormLogin from './FormLogin';

class Login extends Component{

  state = {
    loginErr:''
  }

  handleLoginUser = (e) =>{
    e.preventDefault();
    let form = e.target,
        datos = {
          email:form.email.value,
          password:form.password.value
        }

      LoginUser(datos.email,datos.password)
        .catch(err => {
          this.setState({loginErr:this.setMessage("Correo o Contraseña Icorrecta")})
        })
  }


  setMessage = err => err

  render(){
    return(
      <FormLogin
        loginUser={this.handleLoginUser}
        error={this.state.loginErr}
      />
    )
  }
}

export default Login;