import React from 'react';

import '../../../styles/admin/form.css';

const FormLogin = props => (
  <section className="form-wrapper" onSubmit={props.loginUser}>
    <div className="form-content">
      <h2 className="form-title">Ordep96 Admin</h2>
      <form className="form-ui form">
        <input className="form-ui__field " type="email" name="email" placeholder="Email" />
        <input className="form-ui__field " type="password" name="password" placeholder="Contraseña" />
        {
          props.error !== '' ? <h5>Correo o Contraseña Incorrectos</h5> : ''
        }
        <input className="form-ui__field form-btn" type="submit" value="Entrar" />
      </form>
    </div>
  </section>
);

export default FormLogin;