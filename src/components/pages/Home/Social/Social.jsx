import React from 'react';

const Social = props => (
  <li><a target="_blank" href={props.urlSocial}><i className={`flaticon-${props.name.toLowerCase()}`}></i></a></li>
);


export default Social;