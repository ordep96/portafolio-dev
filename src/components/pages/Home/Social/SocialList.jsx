import React from 'react';

import Social from './Social';

const SocialList = props => (
  <ul className="sociales">
    {
      props.socials.map(social => (
        <Social key={social.socialId} {...social}/>
      ))
    }
  </ul>
);


export default SocialList;