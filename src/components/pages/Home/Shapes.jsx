import React from 'react';

import '../../../styles/shapes.css';

const Shapes = props => (
  <div>
    <svg className="shape shape1" width="700" height="560">
        <polygon points="0,455,693,352,173,0,92,0,0,71" />
    </svg>
    <svg className="shape shape2" width="500" height="460">
        <polygon points="0,0,633,0,633,536" />
    </svg>
  </div>
);


export default Shapes;