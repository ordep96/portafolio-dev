import React from 'react';

import '../../../styles/about.css';

const About = props => (
  <section name="about" className="section">
    <article className="about padd-ordep">
        <h2 className="about__title title">Acerca de mi</h2>
        <p className="about__greeting">Hola, soy Pedro Muñoz Alvear</p>
        <p className="about__description">Tengo 21 años de edad y me desempeño como desarrollador FrontEnd, empecé en este mundo de la programación a los 16 años de edad y me enganche enseguida, y he estado aprendiendo de manera autodidacta. Siempre estoy en constante aprendizaje, Aficionado de los nuevos retos.</p>
    </article>
  </section>
);

export default About;