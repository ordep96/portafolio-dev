import React from 'react';

const HomeLayout = props => (
  <section className="case-page">
    {props.children}
  </section>
)

export default HomeLayout;
