import React from 'react';

const Skill = props => (
  <div  className="skill">
    <div className="skill__info">
      <p>{props.skill}</p>
      <span>{props.percent}%</span>
    </div>
    <div className="percent" style={{'--percent':`${props.percent}%`}}></div>
  </div>
);

export default Skill;