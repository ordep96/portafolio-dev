import React from 'react';

import Skill from './Skill';

import '../../../../styles/skills.css';

const SkillsList = props => (
  <section id="skills" className="section ordep-mtop">
    <article className="skills padd-ordep">
      <h2 className="skills__title title">Personal Skills</h2>
      <section className="skills__tech">
        {
          props.skills.length  > 0
          ?
            props.skills.map(skill => (
              <Skill key={skill.skillId} {...skill} />
            ))
          : null
        }
      </section>
    </article>
  </section>
);


export default SkillsList;