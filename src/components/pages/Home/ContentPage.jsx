import React from 'react';

const ContentPage = props => (
  <section className="ordepContainer">
    <div className="ordep-container">
      {props.children}
    </div>
  </section>
);


export default ContentPage;
