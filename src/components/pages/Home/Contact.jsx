import React from 'react';

import '../../../styles/contact.css';

const Contact = props => (
  <section name="contact" className="section ordep-mtop">
    <article className="contact padd-ordep">
      <h2 className="contact__title title">Contacto</h2>
      <p className="contact__message">No dude en ponerse en contacto conmigo</p>
      <form className="contact__form" onSubmit={props.sendMail}>
        <input className="contact__form__field" type="text" name="name" placeholder="Tu Nombre" required/>
        <input className="contact__form__field" type="email" name="email" placeholder="Tu Email" required/>
        <input className="contact__form__field" type="text" name="subject" placeholder="Asunto" required/>
        <textarea className="contact__form__field textarea" name="message"  cols="30" rows="10" placeholder="Escribe tu mensanje"></textarea>
        <input
          className="contact__form__btnSend"
          type="submit"
          value={props.sendingEmail ? "Enviando...": "Enviar"}
        />
      </form>
      <h2 style={{color:'#fff',marginLeft:'10px'}}>{ props.emailSend ? 'Mensaje Enviado Satisfactoriamente' : '' }</h2>
    </article>
  </section>

)

export default Contact;