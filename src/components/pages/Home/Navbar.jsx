import React from 'react';

import { Link  } from 'react-scroll';


import '../../../styles/navbar.css';

const Navbar = props => (
  <section id="navbar-content" className="sticky" data-margin-top="10" data-sticky-for="1200">
    <div className="navbar-inner" ref={props.navbarInnerRef}>
      <nav className="menu" id="navbar" ref={props.navbarRef} >
        <div className="menu__img">
          <Link  to="about" smooth={true} offset={20} duration={500} data-tooltip="About">
              <img src={props.imgUser} alt="avatar" />
          </Link>
        </div>
        <ul className="menu__list">
          <li>
              <Link to="skills" smooth={true} offset={20} duration={500}  data-tooltip="Skills">
                <i className="flaticon-skills"></i>
              </Link>
          </li>
          <li>
            <Link  to="portfolio" smooth={true} offset={20} duration={500} data-tooltip="Portfolio">
              <i className="flaticon-briefcase"></i>
            </Link>
          </li>
          <li>
            <Link to="contact" smooth={true} offset={20} duration={500} data-tooltip="Contact">
              <i className="flaticon-email"></i>
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  </section>
);


export default Navbar;