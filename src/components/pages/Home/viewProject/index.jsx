import React, { Component } from 'react';
import LoaderPrincipal from '../../../widgets/LoaderPrincipal';

import { firebaseDatabase } from '../../../../data/config';


import DetailsProject from './DetailsProject';

class ViewProject extends Component{
  constructor(...props){
    super(...props)

    this.state = {
      datos:[],
      name:this.props.match.params.name,
      loading:true
    }
  }


  componentDidMount(){

   this.ref = firebaseDatabase().ref('portfolio')
   this.ref.once('value', snapshot => {
    let datos = Object.values(snapshot.val());
      datos.filter(proyect => {
        if(proyect.title.replace(" ","").toLowerCase().includes(this.state.name.toLowerCase())){
          this.setState({datos:proyect,loading:false})
        }
        return false
      })
   })
  }


  componentWillUnmount(){
    this.ref.off();
  }


  render(){
    return(

      this.state.loading
        ? <LoaderPrincipal
          loading={this.state.loading}
        />
        : (
          <DetailsProject
            datos={this.state.datos}
          />
        )
    )
  }
}

export default ViewProject;
