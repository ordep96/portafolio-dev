import React from 'react';


import HeaderProject from './HeaderProject';
import InfoProject from './InfoProject';
import Shapes from '../Shapes';



const detailsProject = props => (
  <div className="case-page">
    <HeaderProject
      title={props.datos.title}
      techs={props.datos.techs}
      subtitle={props.datos.subtitle}
    />
    <InfoProject
      {...props.datos}
    />
    <Shapes />
  </div>
);


export default detailsProject;