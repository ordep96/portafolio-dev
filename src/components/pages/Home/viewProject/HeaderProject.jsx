import React from 'react';

import '../../../../styles/viewProjects/heroProject.css';

const HeaderProject = props => (
  <section className="hero-proyect">
    <div className="ordep-container">
      <div className="hero">
        <h1 className="hero__title">{props.title}</h1>
        <h5 className="hero__tech">
          {
           (typeof props.techs === "object")
              ?
                props.techs.map((tech,i) => {
                  if(i === 0){
                    return tech
                  }
                  return ` | ${tech}`
                })
              : 'No hay Tecnologias'
          }
        </h5>
        <h4 className="hero__review">{props.subtitle}</h4>
      </div>
    </div>
  </section>
);


export default HeaderProject;