import React from 'react';

import '../../../../styles/viewProjects/infoProject.css';


const NotImage = 'https://www.allwoodstairs.com/wp-content/uploads/2015/07/Photo_not_available-4-300x300.jpg';

const InfoProyect =  props => (
  <section className="info-proyect">
    <div className="ordep-container">
      <section className="proyecto">
        <p> {props.firstDescription} </p>

        <img style={{width:'100%'}} src={props.imageDesktop !== 'not-image' ? props.imageDesktop : NotImage} alt="desktop-version" />

        <p>{props.secondDescription}</p>

        <img style={{width:'60%'}} src={props.imageMobile !== 'not-image' ? props.imageMobile : NotImage} alt="mobile-version" />

        {
          props.urlProyect === 'undefined'
            ? ''
            : <a className="go-proyect" target="_blank" href={props.urlProyect}>Ver online</a>
        }

      </section>

    </div>
  </section>
);


export default InfoProyect;