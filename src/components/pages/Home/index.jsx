import React, { Component } from 'react';
import Sticky from 'sticky-js';

import * as emailjs from 'emailjs-com';

import HomeLayout from './HomeLayout';
import ContentPage from './ContentPage';

import Header from './Header';
import SidebarProfile from './SidebarProfile';
import Navbar from './Navbar';

import About from './About';
import Skills from './Skills/SkillsList';
import Portfolio from './Portfolio/PortfolioList';
import Contact from './Contact';
import Shapes from './Shapes';

import LoaderPrincipal from '../../widgets/LoaderPrincipal';

import { firebaseDatabase } from '../../../data/config';

class Home extends Component{
  constructor(...props){
    super(...props)

    this.state = {
      user:null,
      dataProfile:[],
      portfolioPosts:[],
      loading:true,
      emailSend:false,
      sendingEmail:false,

      /* state to skills */
      skills:[],

      /* state to socials */
      socials:[]
    }


  }


  changePositionMenu = () =>{
      if(this.unabled !== 5){
        if(window.innerWidth <= 1198){
          this.navbar.remove();
          this.navbarResponsive.append(this.navbar);
        }else{
          this.navbar.remove();
          this.navbarInner.append(this.navbar);
        }
      }
  }

  handleSendMail = (e) => {
    e.preventDefault();
    let form = e.target,
        datos = {
          name:form.name.value,
          email:form.email.value,
          subject:form.subject.value,
          message:form.message.value
        }

    let templateParams = {
      to_email:"pedro.pmo55@gmail.com",
      subject:datos.subject,
      client_name:datos.name,
      client_mail:datos.email,
      client_message:datos.message
    }

    this.setState({sendingEmail:true})
    emailjs.send('gmail','ordep_template',templateParams,'user_A8jjiachG61Q48djM5SfM')
      .then(response => {
          form.reset();
          this.setState({emailSend:true})
          this.setState({sendingEmail:false})

          this.emailTimer = setTimeout(() =>{
            this.setState({emailSend:false})
          },4000)
      })
  }

  componentDidMount(){

    /* Integrando comportamiento del dom y añadiendo librerias*/
    this.listener = window.addEventListener('resize', this.changePositionMenu.bind(this));

   this.changePositionMenu();

    new Sticky('.sticky');


    /* LLamada de datos */
    this.refPerfil = firebaseDatabase().ref(`user/ordep`)
    this.refPerfil.once('value')
        .then(snapshot => {
          this.setState({dataProfile:snapshot.val()})
    })

    this.refPerfil.on('child_changed', snapshot => {
      console.log(snapshot.val())
      this.setState({dataProfile:snapshot.val()})
    })


    /* referencia a lista de proyectos */
    this.refAdded = firebaseDatabase().ref(`portfolio`)

    this.refAdded.on('child_added',snapshot => {
      this.setState({
        portfolioPosts:this.state.portfolioPosts.concat([snapshot.val()])
      })
    })


    /*  Detectamos cuando ha sido eliminado un Proyecto
    del portafolio*/
    this.refAdded.on('child_removed', oldChildSnapshot => {
      let projectDeleted = oldChildSnapshot.val();
      this.setState({
        portfolioPosts:this.state.portfolioPosts.filter(project => project.postId !== projectDeleted.postId)
      })
    })

    /*  Detectamos si hay un cambio en el portafolio */
    this.refAdded.on('child_changed', snapshot => {
        let ProjectChanged = snapshot.val();
        this.state.portfolioPosts.filter((project,i) => {
          if(project.postId === ProjectChanged.postId){
            let datos = Object.assign(this.state.portfolioPosts);
                datos[i] = ProjectChanged;

             this.setState({ portfolioPosts:datos })
          }

          return false
        })
    })



    /* llamar a lista de habilidaes */
    this.refSkills = firebaseDatabase().ref('skills')
    this.refSkills.on('child_added',snapshot => {
      this.setState({
        skills:this.state.skills.concat([snapshot.val()])
      })
    })


    /* Detectar cuando una habilidad ha sido eliminada */
    this.refSkills.on('child_removed', oldChildSnapshot => {
      let skillDeleted = oldChildSnapshot.val(),
          skillsUpdate = this.state.skills.filter(skill => skill.skillId !== skillDeleted.skillId);

        this.setState({skills:skillsUpdate})
    })


    /* Detectar cuando una habilidad ha sido actualizada */
    this.refSkills.on('child_changed', snapshot => {
       let skillUpdated = snapshot.val();
       this.state.skills.filter((skill,i) => {
         if(skill.skillId === skillUpdated.skillId){
           let skills = Object.assign(this.state.skills);
               skills[i] = skillUpdated;

            this.setState({skills})
         }
         return false
       })
    })


    /* referencia a las a la base de datos para obtener redes sociales */
    this.refSocial = firebaseDatabase().ref('socials')

    /* obtener redes sociales */
    this.refSocial.on('child_added', snapshot => {
      this.setState({
        socials:this.state.socials.concat([snapshot.val()])
      })
    })

    /* Detectar cuando una red Social es actualizada */
    this.refSocial.on('child_changed', snapshot => {
      let socialUpdated = snapshot.val();
      this.state.socials.filter((social,i) => {
        if(social.socialId === socialUpdated.socialId){

          let socials = Object.assign(this.state.socials);
              socials[i] = socialUpdated;

            this.setState({socials})
        }
        return false
      })
    })



      this.wait = setTimeout(() => {
        this.setState({loading:false})
      },6000)

  }


  componentWillUnmount(){
    window.removeEventListener('resize',this.changePositionMenu.bind(this));
    this.unabled = 5;
    this.refAdded.off();
    this.refPerfil.off();
    this.refSkills.off();
    this.refSocial.off();
    clearTimeout(this.wait);
  }


  render(){
    return (
        <HomeLayout className="ordep-wrapper">
          <LoaderPrincipal loading={this.state.loading}/>
          <Header
            navbarResponsiveRef={navbarResponsive => this.navbarResponsive = navbarResponsive}
          />
          <ContentPage>
              <SidebarProfile
                dataProfile={this.state.dataProfile}
                socials={this.state.socials}
              />
            <Navbar
              navbarInnerRef={navbarInner => this.navbarInner = navbarInner}
              navbarRef={navbar => this.navbar = navbar}
              imgUser={this.state.dataProfile.profile_picutre}
            />
            <div id="section-principal" className="ordep-container-sm">
              <About />
              <Skills
                skills={this.state.skills}
              />
              <Portfolio
                portfolioPosts={this.state.portfolioPosts}
              />
              <Contact
                sendMail={this.handleSendMail}
                emailSend={this.state.emailSend}
                sendingEmail={this.state.sendingEmail}
              />
            </div>
          </ContentPage>
          <Shapes />
      </HomeLayout>
    )
  }

}



export default Home;
