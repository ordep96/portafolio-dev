import React from 'react';

import Logo from '../../../media/logo.svg';

import '../../../styles/header.css';

const Header = props => (
    <header id="header">
        <div className="ordep-container">
          <div className="ordep-container-sm">
            <div className="header">
              <img className="header__logo" src={Logo} alt="logo" />
              <span>.Frontend Developer</span>
            </div>
            <div id="navbar-responsive" ref={props.navbarResponsiveRef}></div>
          </div>
        </div>
    </header>

);


export default Header;