import React from 'react';

import SocialList from './Social/SocialList';

import '../../../styles/sidebarProfile.css';

const SidebarProfile = props => {
  const { displayName, username, profile_picutre, career, curriculum } = props.dataProfile;
  return(
    <aside id="profile" className="profile sticky" data-margin-top="10" data-sticky-for="1200">
        <div className="profile-head">
          <div className="profile-head__img">
            <img src={profile_picutre} alt="avatar" />
          </div>
          <div className="profile-head__info">
            <h2 className="name">{displayName}<span>{username}</span></h2>
            <p className="career">{career}</p>
            <SocialList socials={props.socials}/>
          </div>
        </div>
        <div className="profile-body">
          <div className="content-button">
            <a href={curriculum} download>Download Cv</a>
          </div>
        </div>
      </aside>
  )
};


export default SidebarProfile;