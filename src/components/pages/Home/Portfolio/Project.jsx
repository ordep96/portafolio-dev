import React from 'react';
import { Link } from 'react-router-dom';


const NotImage = 'https://www.allwoodstairs.com/wp-content/uploads/2015/07/Photo_not_available-4-300x300.jpg';

const Project = props => (
  <div className="portfolio__grid--item">
    <Link to={`/me/${props.title.replace(" ","")}/`}>
      <img src={props.cover !== 'not-image' ? props.cover : NotImage}  alt={props.title}/>
      <div className="overlay">
        <h4 style={{fontSize:'1.5em'}}>{props.title}</h4>
        <p>
          {
            (typeof props.techs === "object")
              ?
                props.techs.map((tech,i,arr) => {
                  if(i <= 2){
                    return i === 0 ? tech : ` | ${tech}`
                  }else{
                    return ''
                  }
                })
              : 'No hay tecnologias'
          }
        </p>
      </div>
    </Link>
  </div>
);


export default Project;