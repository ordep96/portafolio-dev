import React from 'react';


import Project from './Project';

import '../../../../styles/portfolio.css';

const PortfolioList = props => (
  <section id="portfolio" className="section ordep-mtop">
    <article className="portfolio padd-ordep">
      <h2 className="portfolio__title title">Portafolio</h2>

      <section className="portfolio__grid">
       {
         props.portfolioPosts.map(portfolio => (
            <Project key={portfolio.postId} {...portfolio}/>
         )).reverse()
       }
      </section>

    </article>
  </section>
);


export default PortfolioList;