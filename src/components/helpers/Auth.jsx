import { firebaseAuth, firebaseDatabase, firebaseStorage } from '../../data/config';

const LoginUser = (email,password) => (
  firebaseAuth().signInWithEmailAndPassword(email,password)
);

const Logout = () => firebaseAuth().signOut()



const writeUserData = (name,userName,career,photo,curriculum,userId) => {
    firebaseDatabase().ref(`user/${userId}`).set({
      username:userName,
      displayName:name,
      profile_picutre:photo,
      career:career,
      id:userId,
      curriculum
    })
}

const writePostPortfolio = (title,techs,subtitle,cover,firstDescription,imageDesktop,secondDescription,imageMobile,urlProyect,postId) => (
  firebaseDatabase().ref(`portfolio/${postId}`).set({
     title,
     techs,
     subtitle,
     cover,
     firstDescription,
     imageDesktop,
     secondDescription,
     imageMobile,
     urlProyect,
     postId
  })

)


const updatePostPortfolio = (title,techs,subtitle,cover,firstDescription,imageDesktop,secondDescription,imageMobile,urlProyect,postId) => (
  firebaseDatabase().ref(`portfolio/${postId}`).update({
     title,
     techs,
     subtitle,
     cover,
     firstDescription,
     imageDesktop,
     secondDescription,
     imageMobile,
     urlProyect,
     postId
  })

)

const writeSkill = (skill,percent,skillId) => (
  firebaseDatabase().ref(`skills/${skillId}`).set({
     skill,
     percent,
     skillId
  })
)


const updateSkill = (skill,percent,skillId) => (
  firebaseDatabase().ref(`skills/${skillId}`).update({
    skill,
    percent,
    skillId
  })
)

const writeSocialMedia = (name,urlSocial,socialId) => (
  firebaseDatabase().ref(`socials/${socialId}`).set({
    name,
    urlSocial,
    socialId
  })
)

const updateSocial = (name,urlSocial,socialId) => (
  firebaseDatabase().ref(`socials/${socialId}`).update({
    name,
    urlSocial,
    socialId
  })
)

const deleteImgs = (imgs = []) => (
  imgs.map(img => {
    if(img !== 'not-image'){
      let ref = firebaseStorage().refFromURL(img);
          ref.delete();
    }
    return false
  })
)

export {
  LoginUser,
  Logout,
  writeUserData,
  writePostPortfolio,
  updatePostPortfolio,
  writeSkill,
  updateSkill,
  writeSocialMedia,
  updateSocial,
  deleteImgs,
}