import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

const config = {
  apiKey: "api key",
  authDomain: "auth domain",
  databaseURL: "database url",
  projectId: "project id",
  storageBucket: "storage bucket",
  messagingSenderId: "messaging"
};

firebase.initializeApp(config);

export const firebaseAuth = firebase.auth;
export const firebaseDatabase = firebase.database;
export const firebaseStorage = firebase.storage;