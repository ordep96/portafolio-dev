import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './styles/icons/flaticon.css';
import App from './components/';
import ErrorWrapper from './components/error/';


import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(
  <ErrorWrapper>
     <App/>
  </ErrorWrapper>,
  document.getElementById('root'));
registerServiceWorker();
